import angular from 'angular';
import uiRouter from 'angular-ui-router';
import routing from './main.routes';

export class MainController {
  awesomeThings = [];
  newThing = '';

  /*@ngInject*/
  constructor(Auth, $location) {
    'ngInject';
    //direct user to the right page
    Auth.isLoggedIn().then(ret => {
      if(ret == '') {
        $location.path('/login');
      } else {
        if(ret == 'admin') {
          $location.path('/admin');
        }
        if(ret == 'teacher') {
          $location.path('/managestudent');
        }
      }
    });
  }
}

export default angular.module('fractionBackendApp.main', [uiRouter])
  .config(routing)
  .component('main', {
    template: require('./main.pug'),
    controller: MainController
  })
  .name;
