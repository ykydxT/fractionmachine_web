'use strict';

export default class AdminController {
  /*@ngInject*/
  constructor(User) {
    // Use the User $resource to fetch all users
    this.users = User.query();
  }
  delete(user) {
    if(confirm('Do you really want to delete ' + user.name + '?') == true) {
      user.$remove();
      this.users.splice(this.users.indexOf(user), 1);
    }
  }
}
