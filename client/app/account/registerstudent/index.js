'use strict';

import angular from 'angular';
import RegisterstudentCtrl from './registerstudent.controller';

export default angular.module('fractionBackendApp.registerstudent', [])
  .controller('RegisterstudentCtrl', RegisterstudentCtrl)
  .name;
