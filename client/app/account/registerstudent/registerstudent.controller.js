'use strict';

//import angular from 'angular';

export default class RegisterstudentCtrl {
  student = {
    name: '',
    sid: '',
    classname: '',
    year: '',
    gender: '',
    school: '',
    teacher: ''
  };
  errors = {};
  submitted = false;
  norepeat = true;


  /*@ngInject*/
  constructor(Auth, $state) {
    this.Auth = Auth;
    this.$state = $state;
    this.validity = false;
    this.getCurrentUser = Auth.getCurrentUserSync;
  }
  register(form) {
    this.submitted = true;
    if(form.$valid) {
      this.Auth.createStudent({
        name: this.student.name,
        sid: this.student.sid,
        classname: this.student.classname,
        school: this.getCurrentUser().school,
        year: this.student.year,
        gender: this.student.gender,
        teacher: this.getCurrentUser()._id
      })
        .then(() => {
          alert('Registration for ' + this.student.name + ' succeeded!');
          location.reload();
        })
        // if the backend responses that student id is repeated in on class, alert error
        .catch(err => {
          var patt = new RegExp('sameclass');
          if(patt.test(err.data.message)) {
            alert('Same student id in one class!');
            this.norepeat = false;
          }
        });
    }
  }
}
