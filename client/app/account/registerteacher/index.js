'use strict';

import angular from 'angular';
import RegisterteacherCtrl from './registerteacher.controller';

export default angular.module('fractionBackendApp.registerteacher', [])
  .controller('RegisterteacherCtrl', RegisterteacherCtrl)
  .name;
