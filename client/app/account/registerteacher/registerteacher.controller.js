'use strict';

//import angular from 'angular';

export default class RegisterteacherCtrl {

  user = {
    name: '',
    school: '',
    email: '',
  };
  errors = {};
  submitted = false;


    /*@ngInject*/
  constructor(Auth, $state, $http) {
    this.Auth = Auth;
    this.$state = $state;
    this.$http = $http;
  }

  register(form) {
    this.submitted = true;

    if(form.$valid) {
      this.Auth.createUser({
        name: this.user.name,
        school: this.user.school,
        email: this.user.email,
        role: 'teacher'
      })
        .then(() => {
          alert('Registration for ' + this.user.name + ' succeeded!');
          location.reload();
        })
        .catch(err => {
          // cannot have repeated email
          var patt1 = new RegExp('UsedEmail');
          if(patt1.test(err.data.message)) {
            alert('The specific email address is already in use!');
          }
          err = err.data;
          this.errors = {};
        });
    }
  }

  confirm(form) {
    if(form.$valid) {
      location.reload();
    }
  }
}

