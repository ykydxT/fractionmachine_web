'use strict';
export default class ForgetpasswordCtrl {
  // only email is used for forgetpassword
  user = {
    email: ''
  };
  errors = {};
  submitted = false;

  /*@ngInject*/
  constructor(Auth, $state, $http) {
    this.Auth = Auth;
    this.$state = $state;
    this.validity = false;
    this.$http = $http;
  }

  forget(form) {
    this.submitted = true;
    if(form.$valid) {
      this.validity = true;
      return this.$http.put('/api/fractionusers/forgetpwd', {email: this.user.email});
    }
  }

  confirm(form) {
    if(form.$valid) {
      alert('Please check the email');
    }
  }
}
