'use strict';

import angular from 'angular';
import ForgetpasswordCtrl from './forgetpassword.controller';

export default angular.module('fractionBackendApp.forgetpassword', [])
  .controller('ForgetpasswordCtrl', ForgetpasswordCtrl)
  .name;
