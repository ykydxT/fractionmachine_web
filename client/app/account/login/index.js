'use strict';

import angular from 'angular';
import LoginCtrl from './login.controller';

export default angular.module('fractionBackendApp.login', [])
  .controller('LoginCtrl', LoginCtrl)
  .name;
