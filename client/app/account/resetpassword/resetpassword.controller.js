'use strict';

export default class ResetpasswordCtrl {
  user = {
    newPassword: '',
    confirmPassword: ''
  };
  errors = {
    other: undefined
  };
  message = '';
  submitted = false;
  /*@ngInject*/
  constructor(Auth, $state, $stateParams, $http) {
    this.Auth = Auth;
    this.$state = $state;
    // retrieve reset token from URL
    this.resettoken = $stateParams.resettoken;
    this.$http = $http;
  }
  resetPassword(form) {
    this.submitted = true;
      if(form.$valid) {
        this.$http.put('/api/fractionusers/resetpassword/' + this.resettoken, {resettoken: this.resettoken, newpwd: this.user.newPassword})
          .then(() => {
            this.message = 'Password successfully changed.';
            alert('Password changed!');
            this.$state.go('main');
          })
          .catch((err) => {
            alert(err.data);
          });
      }
  }
}
