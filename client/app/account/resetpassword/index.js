'use strict';

import angular from 'angular';
import ResetpasswordCtrl from './resetpassword.controller';

export default angular.module('fractionBackendApp.resetpassword', [])
  .controller('ResetpasswordCtrl', ResetpasswordCtrl)
  .name;
