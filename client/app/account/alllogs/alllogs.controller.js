'use strict';

export default class AlllogsCtrl {
  /*@ngInject*/
  // list all the students in the db
  constructor(Student) {
    this.students = Student.query();
    console.log(this.students);
  }

  download(student) {
    alert('Download start!');
    let url = 'api/logs/single?ssid=' + student._id;
    window.open(url);
  }
}
