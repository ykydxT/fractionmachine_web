'use strict';

import angular from 'angular';
import AlllogsCtrl from './alllogs.controller';

export default angular.module('fractionBackendApp.alllogs', [])
  .controller('AlllogsCtrl', AlllogsCtrl)
  .name;
