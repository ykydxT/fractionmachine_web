'use strict';

export default function routes($stateProvider) {
  'ngInject';

  $stateProvider.state('login', {
    url: '/login',
    template: require('./login/login.pug'),
    controller: 'LoginCtrl',
    controllerAs: 'vm'
  })
    .state('logout', {
      url: '/logout?referrer',
      referrer: 'main',
      template: '',
      controller($state, Auth) {
        'ngInject';

        var referrer = $state.params.referrer || $state.current.referrer || 'main';
        Auth.logout();
        $state.go(referrer);
      }
    })
    // admin can register teachers
    .state('registerteacher', {
      url: '/registerteacher',
      template: require('./registerteacher/registerteacher.pug'),
      controller: 'RegisterteacherCtrl',
      controllerAs: 'vm',
      authenticate: true
    })
    // change password
    .state('settings', {
      url: '/settings',
      template: require('./settings/settings.pug'),
      controller: 'SettingsController',
      controllerAs: 'vm',
      authenticate: true
    })
    // teachers can register students
    .state('registerstudent', {
      url: '/registerstudent',
      template: require('./registerstudent/registerstudent.pug'),
      controller: 'RegisterstudentCtrl',
      controllerAs: 'vm',
      authenticate: true
    })
    // teachers can delete students and download student logs
    .state('managestudent', {
      url: '/managestudent',
      template: require('./managestudent/managestudent.pug'),
      controller: 'ManagestudentCtrl',
      controllerAs: 'vm',
      authenticate: true
    })
    // if forget password, user can reset password
    .state('forgetpassword', {
      url: '/forgetpassword',
      template: require('./forgetpassword/forgetpassword.pug'),
      controller: 'ForgetpasswordCtrl',
      controllerAs: 'vm',
    })
    // admin have access to all logs
    .state('alllogs', {
      url: '/alllogs',
      template: require('./alllogs/alllogs.pug'),
      controller: 'AlllogsCtrl',
      controllerAs: 'vm',
      authenticate: true
    })
    // reset password with a resettoken
    .state('resetpassword', {
      url: '/resetpassword/:resettoken',
      template: require('./resetpassword/resetpassword.pug'),
      controller: 'ResetpasswordCtrl',
      controllerAs: 'vm',
      authenticate: false
    })
    ;
}
