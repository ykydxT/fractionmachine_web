'use strict';

import angular from 'angular';

import uiRouter from 'angular-ui-router';

import routing from './account.routes';
import login from './login';
import settings from './settings';
import registerteacher from './registerteacher';
import registerstudent from './registerstudent';
import managestudent from './managestudent';
import forgetpassword from './forgetpassword';
import alllogs from './alllogs';
import resetpassword from './resetpassword';

export default angular.module('fractionBackendApp.account', [resetpassword, uiRouter, settings, registerteacher, registerstudent, managestudent, forgetpassword, login, alllogs])
  .config(routing)
  .run(function($rootScope) {
    'ngInject';

    $rootScope.$on('$stateChangeStart', function(event, next, nextParams, current) {
      if(next.name === 'logout' && current && current.name && !current.authenticate) {
        next.referrer = current.name;
      }
    });
  })
  .name;
