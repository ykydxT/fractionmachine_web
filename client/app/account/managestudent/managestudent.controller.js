export default class ManagestudentCtrl {
  /*@ngInject*/
  constructor($http, Auth, Student, $cookies) {
    this.$http = $http;
    this.$cookies = $cookies;
    Auth.getCurrentUser()
      .then(user => {
        this.user = user;
        Student.query({teacher: user._id}, studentq => {
          this.students = studentq;
          var students_class = {};
          // group the students by classes
          for(var i in studentq) {
            if(studentq[i].classname != undefined) {
              var classnn = studentq[i].classname;
              if(classnn in students_class) {
                students_class[classnn].push(studentq[i]);
              }
              else {
                students_class[classnn] = [studentq[i]];
              }
            }
          }
          this.students_class = students_class;
        });
      })
      .catch(err => {
        console.log(err);
      });
  }
  download(classname) {
    alert('Download start!');
    let url = 'api/logs?class=' + classname + '&teacher=' + this.user._id;
    window.open(url);
  }
  delete(student) {
    if(confirm('Do you really want to delete this student? ' + student.name) == true) {
      var class_name = student.classname;
      this.students_class[class_name].splice(this.students_class[class_name].indexOf(student), 1);
      if(this.students_class[class_name].length == 0) {
        delete this.students_class[class_name];
      }
      student.$remove();
    }
  }
}
