'use strict';

import angular from 'angular';
import ManagestudentCtrl from './managestudent.controller';

export default angular.module('fractionBackendApp.managestudent', [])
  .controller('ManagestudentCtrl', ManagestudentCtrl)
  .name;
