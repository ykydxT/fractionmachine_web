'use strict';

export function StudentResource($resource) {
  'ngInject';

  return $resource('/api/students/:id/:controller', {
    id: '@_id'
  }, {
    get: {
      method: 'GET',
      params: {
        id: 'me'
      }
    }
  });
}
