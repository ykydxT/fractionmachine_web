'use strict';
import express from 'express';
import config from '../config/environment';
import Fractionuser from '../api/fractionuser/fractionuser.model';
// Passport Configuration
require('./local/passport').setup(Fractionuser, config);

var router = express.Router();

router.use('/local', require('./local').default);

export default router;
