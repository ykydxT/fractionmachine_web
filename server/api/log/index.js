'use strict';

import * as auth from '../../auth/auth.service';

var express = require('express');
var controller = require('./log.controller');

var router = express.Router();


router.get('/single/', auth.isAuthenticated(), controller.download_single);

router.get('/', auth.isAuthenticated(), controller.download);

router.post('/fromapp/:sid', auth.isAuthenticated(), controller.insert);
// router.put('/:id', controller.upsert);
// router.patch('/:id', controller.patch);
// router.delete('/:id', controller.destroy);

module.exports = router;
