'use strict';

import mongoose, {Schema} from 'mongoose';
import Student from '../student/student.model';
import {registerEvents} from './log.events';
var subSchema = new Schema({
  action: String,
  input: String,
  output: String,
  numerator: String,
  denominator: String,
  time: String
}, {_id: false});

var LogSchema = new Schema({
  ssid: {
    type: Schema.Types.ObjectId,
    ref: 'Student',
    required: [true, 'log should have student id!']
  },
  contents: [subSchema]
});

registerEvents(LogSchema);
export default mongoose.model('Log', LogSchema);
