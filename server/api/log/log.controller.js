/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/logs              ->  index
 * POST    /api/logs              ->  create
 * GET     /api/logs/:id          ->  show
 * PUT     /api/logs/:id          ->  upsert
 * PATCH   /api/logs/:id          ->  patch
 * DELETE  /api/logs/:id          ->  destroy
 */

'use strict';

import jsonpatch from 'fast-json-patch';
import Log from './log.model';
var mongoose = require('mongoose');
var fs = require('fs');
var nodexl = require('node-xlsx');
var json2csv = require('json2csv');
import Student from '../student/student.model';

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if(entity) {
      return res.status(statusCode).json(entity);
    }
    return null;
  };
}

// respond with Excel file of students in a class, each sheet stands for one student. 
// inefficient!!! needs fix!!!
function respondcsv(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if(entity) {
      var xls_format_all = [];
      var student_id = [];
      entity.forEach(i => {
        student_id.push(i._id);
      });
      Log.find({ssid: {$in: student_id}}, '-_id ssid contents').exec()
        .then((logss => {
          logss.forEach(j => {
            entity.forEach(k => {
              if(k._id.toString() == j.ssid.toString()) {
                var log_data = [['action', 'input', 'output', 'numerator', 'denominator', 'time']];
                j.contents.forEach(each_log => {
                  log_data.push([each_log.action, each_log.input, each_log.output, each_log.numerator, each_log.denominator, each_log.time]);
                });
                xls_format_all.push({name: k.sid + '_' + k.name, data: log_data});
              }
            });
          });
          var buffer = nodexl.build(xls_format_all);
          var tem_file = 'class_' + student_id[0] + entity[0].classname + '_log.xlsx';
          fs.writeFile(tem_file, buffer, {flag: 'w'}, err => {
            if(err) {
              console.log(err);
            }
            res.download(tem_file, 'class_' + entity[0].classname + '_log.xlsx', err => {
              if(err) {
                console.log(err);
              }
              fs.unlink(tem_file);
            });
          });
        }));
    }
    return null;
  };
}

// respond with Excel file of a single student
function respondcsv_single(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if(entity) {
      console.log(entity);
      var jsonc = JSON.parse(JSON.stringify(entity))[0].contents;

      var studentinfo = JSON.parse(JSON.stringify(entity))[0].ssid;

      var logheader = ['action', 'input', 'output', 'numerator', 'denominator', 'time'];

      var keyss = Object.keys(studentinfo).concat(logheader);

      jsonc.unshift(studentinfo, (err) => {
        console.log(err);
      });
      console.log(jsonc);
      var csvfile = json2csv({data: jsonc, fields: keyss});
      console.log(csvfile);
      var tem_file = '/tmp/' + entity.ssid + '.csv';
      fs.writeFile(tem_file, csvfile, err => {
        if(err) {
          console.log(err);
        }
        res.download(tem_file, studentinfo.name + '_log.csv', err => {
          if(err) {
            console.log(err);
          }
          fs.unlink(tem_file);
        });
      });
    }
  };
}

function patchUpdates(patches) {
  return function(entity) {
    try {
      // eslint-disable-next-line prefer-reflect
      jsonpatch.apply(entity, patches, /*validate*/ true);
    } catch(err) {
      return Promise.reject(err);
    }

    return entity.save();
  };
}

function removeEntity(res) {
  return function(entity) {
    if(entity) {
      return entity.remove()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if(!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

// Get a list of Logs
export function index(req, res) {
  return Log.find().exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}


// Gets Logs of one class from the DB given a teacherid and classname
export function download(req, res) {
  var classname = req.query.class;
  var teacher_id = req.query.teacher;
  return Student.find({classname: classname, teacher: teacher_id}, '_id name year gender sid classname').exec()
    .then(handleEntityNotFound(res))
    .then(respondcsv(res))
    .catch(handleError(res));
}

// Gets Logs from the DB given the sid of a student
export function download_single(req, res) {
  return Log.find({ssid: req.query.ssid}, '-_id ssid contents').populate('ssid', '-_id name year gender sid classname').exec()
    .then(handleEntityNotFound(res))
    .then(respondcsv_single(res))
    .catch(handleError(res));
}

//Insert logs into database
export function insert(req, res) {
  var sid = mongoose.Types.ObjectId(req.params.sid);
  console.log(sid);
  var contents = JSON.parse(JSON.stringify(req.body)).Items;
  console.log(contents);
  Student.count({_id: sid}, (err, count) => {
    if(count == 1) {
      return Log.update({ssid: sid}, {$push: {contents: {$each: contents}}}, {upsert: true}).exec()
      .then(() => {
        res.status(200).end();
      })
      .catch(err => {
        console.log('fail to store log' + err);
        res.status(501).send('storing log fails!');
      });
    }
    else {
      res.status(403).send('not such student!');
    }
  });
}

// Creates a new Log in the DB
export function create(req, res) {
  return Log.create(req.body)
    .then(respondWithResult(res, 201))
    .catch(handleError(res));
}

// Upserts the given Log in the DB at the specified ID
export function upsert(req, res) {
  if(req.body._id) {
    Reflect.deleteProperty(req.body, '_id');
  }
  return Log.findOneAndUpdate({_id: req.params.id}, req.body, {new: true, upsert: true, setDefaultsOnInsert: true, runValidators: true}).exec()

    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Updates an existing Log in the DB
export function patch(req, res) {
  if(req.body._id) {
    Reflect.deleteProperty(req.body, '_id');
  }
  return Log.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(patchUpdates(req.body))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Deletes a Log from the DB
export function destroy(req, res) {
  return Log.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}
