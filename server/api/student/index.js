'use strict';
import * as auth from '../../auth/auth.service';


var express = require('express');
var controller = require('./student.controller');

var router = express.Router();

router.get('/fromapp', auth.isAuthenticated(), controller.indexsid);
router.get('/', auth.isAuthenticated(), controller.index);
router.get('/:id', auth.isAuthenticated(), controller.show);
router.post('/', auth.isAuthenticated(), controller.create);
router.delete('/:id', auth.isAuthenticated(), controller.destroy);

module.exports = router;
