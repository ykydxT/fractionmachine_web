'use strict';

import mongoose, {Schema} from 'mongoose';
import {registerEvents} from './student.events';

var StudentSchema = new Schema({
  name: {
    type: String,
    reuqired: [true, 'student should have name!']
  },
  sid: {
    type: String,
    reuqired: [true, 'student should have login id!']
  },
  classname: {
    type: String,
    reuqired: [true, 'student should have class name!']
  },
  school: {
    type: String,
    reuqired: [true, 'student should have school!']
  },
  year: {
    type: Number,
    min: 0,
    max: 12
  },
  gender: {
    type: String,
    enum: ['male', 'female']
  },
  teacher: {
    type: Schema.Types.ObjectId,
    ref: 'Teacher',
    reuqired: [true, 'student should have teacher!']
  },
  registrationdate: {
    type: Date,
    default: Date.now
  }
});
//For a teacher, cannot have two same student id in one class
StudentSchema
  .path('sid')
  .validate(function(value) {
    return this.constructor.find({ sid: value }).exec()
      .then(students => {
        if(students) {
          for(var i in students) {
            var student = students[i];
            if(this.teacher === student.teacher && this.classname === student.classname) {
              return false;
            }
          }
          return true;
        }
        return true;
      })
      .catch(function(err) {
        throw err;
      });
  }, 'sameclass');

registerEvents(StudentSchema);
export default mongoose.model('Student', StudentSchema);
