/**
 * Fractionuser model events
 */

'use strict';

import {EventEmitter} from 'events';
var FractionuserEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
FractionuserEvents.setMaxListeners(0);

// Model events
var events = {
  save: 'save',
  remove: 'remove'
};

// Register the event emitter to the model events
function registerEvents(Fractionuser) {
  for(var e in events) {
    let event = events[e];
    Fractionuser.post(e, emitEvent(event));
  }
}

function emitEvent(event) {
  return function(doc) {
    FractionuserEvents.emit(event + ':' + doc._id, doc);
    FractionuserEvents.emit(event, doc);
  };
}

export {registerEvents};
export default FractionuserEvents;
