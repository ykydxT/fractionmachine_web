'use strict';

import config from '../../config/environment';
import Fractionuser from './fractionuser.model';
import Student from '../student/student.model.js';
import jwt from 'jsonwebtoken';
import {signResettoken} from '../../auth/auth.service';

const sendgridkey = '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';
const sgMail = require('@sendgrid/mail');


function validationError(res, statusCode) {
  statusCode = statusCode || 422;
  return function(err) {
    return res.status(statusCode).json(err);
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    return res.status(statusCode).send(err);
  };
}

// after registering a teacher, send an email to notify the teacher
function sendemail(pwd, user) {
  sgMail.setApiKey(sendgridkey);
  var data = {
    from: 'noreply <noreply@fractionmachine.com.au>',
    to: user.email,
    subject: 'Welcome to Fraction Machine',
    text: 'Welcome to Fraction Machine, ' + user.name + '! Your password is:' + pwd + '<br>Please click on the link below to login :<br><br><a href="http://115.146.92.79/:3000/',
    html: 'Welcome to Fraction Machine, ' + user.name + '! Your password is:' + pwd + '<br>Please click on the link below to login: <br><br><a href="http://115.146.92.79:3000/">http://115.146.92.79:3000/</a>'
  };
  sgMail.send(data, function(err) {
    if(err) {
      console.log('sending error');
      console.log(err);
    }
  });
}

// when a user forget a password, send an email to his/her email box
function sendforgetemail(user) {
  console.log('sending a forget email');
  sgMail.setApiKey(sendgridkey);
  var data = {
    from: 'noreply <noreply@fractionmachine.com.au>',
    to: user.email,
    subject: 'Reset Password',
    text: 'Hello ' + user.name + ', You recently request a password reset link. Please click on the link below to reset your password:<br><br><a href="http://115.146.92.79/:3000/resetpassword/' + user.resettoken,

    html: 'Hello<strong> ' + user.name + '</strong>,<br><br>You recently request a password reset link. Please click on the link below to reset your password:<br><br><a href="http://115.146.92.79:3000/resetpassword/' + user.resettoken + '">http://115.146.92.79:3000/resetpassword/</a>'

  };
  sgMail.send(data, function(err) {
    if(err) {
      console.log(err);
    }
    return true;
  });
}


/**
 * Get list of users
 * restriction: 'admin'
 */
export function index(req, res) {
  return Fractionuser.find({role: 'teacher'}, '-salt -password').exec()
    .then(users => {
      return res.status(200).json(users);
    })
    .catch(handleError(res));
}

/**
 * Creates a new user
 */
export function create(req, res) {
  console.log('creating users...');
  var newUser = new Fractionuser(req.body);
  // randomly generate password
  var new_pwd = Math.random().toString(36).substr(5);

  newUser.password = new_pwd;

  newUser.provider = 'local';
  return newUser.save()
    .then(() => {
      return sendemail(new_pwd, newUser);
    })
    .then(() => {
      return res.status(200).send('register successfully!');
    })
    .catch(handleError(res));
}

/**
 * Get a single user
 */
export function show(req, res, next) {
  var userId = req.params.id;

  return Fractionuser.findById(userId).exec()
    .then(user => {
      if(!user) {
        return res.status(404).end();
      }
      res.json(user.profile);
    })
    .catch(err => next(err));
}

/**
 * Deletes a user
 * restriction: 'admin'
 */
export function destroy(req, res) {
  return Fractionuser.findByIdAndRemove(req.params.id).exec()
    .then(() => {
      return Student.remove({teacher: req.params.id});
    })
    .then(() => {
      return res.status(200).end();
    })
    .catch(err => {
      console.log(err.data);
      res.status(501).end();
    });
  // return Fractionuser.findByIdAndRemove(req.params.id, function(err){
  //   console.log(err);

  // })
}

/**
 * Change a users password
 */
export function changePassword(req, res) {
  var userId = req.user._id;
  var oldPass = String(req.body.oldPassword);
  var newPass = String(req.body.newPassword);

  return Fractionuser.findById(userId).exec()
    .then(user => {
      if(user.authenticate(oldPass)) {
        user.password = newPass;
        return user.save()
          .then(() => {
            res.status(204).end();
          })
          .catch(validationError(res));
      } else {
        return res.status(403).end();
      }
    });
}

/**
 * when a user forgets password, send email, insert a resettoken
 */
export function forgetpwd(req, res) {
  return Fractionuser.findOne({email: req.body.email}).select('name email resettoken').exec()
    .then(user => {
      if(!user) {
        res.status(401).end();
      }
      else {
        res.json(200);
        user.resettoken = signResettoken(user.name, user.email);
        user.save(err => {
          if(err) {
            res.json(err);
          }
          else {
            sendforgetemail(user);
          }
        });
      }
    })
    .catch(err => {
      console.log(err.data);
      res.status(501).end();
    });
}

// if a user forgets password, reset the password with a link in email
export function resetpwd(req, res) {
  return Fractionuser.findOne({resettoken: req.params.resettoken}).exec()
    .then(user => {
      if(!user) {
        res.status(401).send('Wrong user or you have changed your password!');
      }
      else {
        // verify the resettoken
        console.log('verifying the token');
        jwt.verify(req.params.resettoken, config.secrets.session, function(err) {
          if(err) {
            res.status(401).send('Token expired!');
          }
          else {
            user.resettoken = false;
            user.password = req.body.newpwd;
            user.save(function(err) {
              if(err) {
                res.res.status(401).send('Save password failed!');
              }
              else {
                res.status(200).send('Password changed!');
              }
            });
          }
        });
      }
    })
    .catch(err => {
      console.log(err);
      res.status(401).send('Something wrong!');
    });
}

// }

/**
 * Get my info
 */
export function me(req, res, next) {
  var userId = req.user._id;

  return Fractionuser.findOne({ _id: userId }, '-salt -password').exec()
    .then(user => { // don't ever give out the password or salt
      if(!user) {
        return res.status(401).end();
      }
      return res.json(user);
    })
    .catch(err => next(err));
}

/**
 * Authentication callback
 */
export function authCallback(req, res) {
  res.redirect('/');
}

