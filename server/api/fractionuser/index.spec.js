'use strict';

/* globals sinon, describe, expect, it */

var proxyquire = require('proxyquire').noPreserveCache();

var fractionuserCtrlStub = {
  index: 'fractionuserCtrl.index',
  show: 'fractionuserCtrl.show',
  create: 'fractionuserCtrl.create',
  upsert: 'fractionuserCtrl.upsert',
  patch: 'fractionuserCtrl.patch',
  destroy: 'fractionuserCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var fractionuserIndex = proxyquire('./index.js', {
  express: {
    Router() {
      return routerStub;
    }
  },
  './fractionuser.controller': fractionuserCtrlStub
});

describe('Fractionuser API Router:', function() {
  it('should return an express router instance', function() {
    fractionuserIndex.should.equal(routerStub);
  });

  describe('GET /api/fractionusers', function() {
    it('should route to fractionuser.controller.index', function() {
      routerStub.get
        .withArgs('/', 'fractionuserCtrl.index')
        .should.have.been.calledOnce;
    });
  });

  describe('GET /api/fractionusers/:id', function() {
    it('should route to fractionuser.controller.show', function() {
      routerStub.get
        .withArgs('/:id', 'fractionuserCtrl.show')
        .should.have.been.calledOnce;
    });
  });

  describe('POST /api/fractionusers', function() {
    it('should route to fractionuser.controller.create', function() {
      routerStub.post
        .withArgs('/', 'fractionuserCtrl.create')
        .should.have.been.calledOnce;
    });
  });

  describe('PUT /api/fractionusers/:id', function() {
    it('should route to fractionuser.controller.upsert', function() {
      routerStub.put
        .withArgs('/:id', 'fractionuserCtrl.upsert')
        .should.have.been.calledOnce;
    });
  });

  describe('PATCH /api/fractionusers/:id', function() {
    it('should route to fractionuser.controller.patch', function() {
      routerStub.patch
        .withArgs('/:id', 'fractionuserCtrl.patch')
        .should.have.been.calledOnce;
    });
  });

  describe('DELETE /api/fractionusers/:id', function() {
    it('should route to fractionuser.controller.destroy', function() {
      routerStub.delete
        .withArgs('/:id', 'fractionuserCtrl.destroy')
        .should.have.been.calledOnce;
    });
  });
});
