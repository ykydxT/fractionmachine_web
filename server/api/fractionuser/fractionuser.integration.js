'use strict';

/* globals describe, expect, it, beforeEach, afterEach */

var app = require('../..');
import request from 'supertest';

var newFractionuser;

describe('Fractionuser API:', function() {
  describe('GET /api/fractionusers', function() {
    var fractionusers;

    beforeEach(function(done) {
      request(app)
        .get('/api/fractionusers')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          fractionusers = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      fractionusers.should.be.instanceOf(Array);
    });
  });

  describe('POST /api/fractionusers', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/fractionusers')
        .send({
          name: 'New Fractionuser',
          info: 'This is the brand new fractionuser!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          newFractionuser = res.body;
          done();
        });
    });

    it('should respond with the newly created fractionuser', function() {
      newFractionuser.name.should.equal('New Fractionuser');
      newFractionuser.info.should.equal('This is the brand new fractionuser!!!');
    });
  });

  describe('GET /api/fractionusers/:id', function() {
    var fractionuser;

    beforeEach(function(done) {
      request(app)
        .get(`/api/fractionusers/${newFractionuser._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          fractionuser = res.body;
          done();
        });
    });

    afterEach(function() {
      fractionuser = {};
    });

    it('should respond with the requested fractionuser', function() {
      fractionuser.name.should.equal('New Fractionuser');
      fractionuser.info.should.equal('This is the brand new fractionuser!!!');
    });
  });

  describe('PUT /api/fractionusers/:id', function() {
    var updatedFractionuser;

    beforeEach(function(done) {
      request(app)
        .put(`/api/fractionusers/${newFractionuser._id}`)
        .send({
          name: 'Updated Fractionuser',
          info: 'This is the updated fractionuser!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          updatedFractionuser = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedFractionuser = {};
    });

    it('should respond with the updated fractionuser', function() {
      updatedFractionuser.name.should.equal('Updated Fractionuser');
      updatedFractionuser.info.should.equal('This is the updated fractionuser!!!');
    });

    it('should respond with the updated fractionuser on a subsequent GET', function(done) {
      request(app)
        .get(`/api/fractionusers/${newFractionuser._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          let fractionuser = res.body;

          fractionuser.name.should.equal('Updated Fractionuser');
          fractionuser.info.should.equal('This is the updated fractionuser!!!');

          done();
        });
    });
  });

  describe('PATCH /api/fractionusers/:id', function() {
    var patchedFractionuser;

    beforeEach(function(done) {
      request(app)
        .patch(`/api/fractionusers/${newFractionuser._id}`)
        .send([
          { op: 'replace', path: '/name', value: 'Patched Fractionuser' },
          { op: 'replace', path: '/info', value: 'This is the patched fractionuser!!!' }
        ])
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          patchedFractionuser = res.body;
          done();
        });
    });

    afterEach(function() {
      patchedFractionuser = {};
    });

    it('should respond with the patched fractionuser', function() {
      patchedFractionuser.name.should.equal('Patched Fractionuser');
      patchedFractionuser.info.should.equal('This is the patched fractionuser!!!');
    });
  });

  describe('DELETE /api/fractionusers/:id', function() {
    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete(`/api/fractionusers/${newFractionuser._id}`)
        .expect(204)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when fractionuser does not exist', function(done) {
      request(app)
        .delete(`/api/fractionusers/${newFractionuser._id}`)
        .expect(404)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });
  });
});
