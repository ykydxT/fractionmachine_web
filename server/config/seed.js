/**
 * Populate DB with sample data on server start
 * to disable, edit config/environment/index.js, and set `seedDB: false`
 */

'use strict';

import Fractionuser from '../api/fractionuser/fractionuser.model';
import config from './environment/';

export default function seedDatabaseIfNeeded() {
  if(config.seedDB) {
    Fractionuser.find({}).remove()
      .then(() => {
        Fractionuser.create({
          provider: 'local',
          role: 'admin',
          name: 'Admin',
          email: 'robert.hunting@unimelb.edu.au',
          password: 'mnsi_fraction',
          school: 'UOM'
        })
        .then(() => console.log('finished populating fraction users'))
        .catch(err => console.log('error populating users', err));
      });
  }
}
